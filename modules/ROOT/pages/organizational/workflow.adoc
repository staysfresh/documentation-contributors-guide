= The Docs Workflow organization

The Fedora Docs team agreed on a workflow organization, which converges at the two central pages:

. https://gitlab.com/groups/fedora/docs/-/boards[the GitLab dashboard] 

. https://gitlab.com/groups/fedora/docs/-/merge_requests[the GitLab Open Merge Requests]

NOTE: This workflow organization is relevant for Docs members or people who want to become Docs members. Casual contributors, who just want to contribute to Docs content from time to time, should read the 7-step HowTo.

== The dashboard lists and labels

There are *four labels that classify the state of an issue ticket*:

* Triage: 
Issue is labeled & classified, but no one has been assigned yet. Do you want to take over? Feel free to assign yourself and shift the issue to “In progress”!
* In progress: 
Someone has been assigned to this issue and it is in progress!
* Support needed: 
Something was already done, but someone has to support the existing assignee OR take over the assignment at all (see comments within the issue)!
* Approval needed: 
This is a major change and it needs an approval from someone else than the assignee! Feel free to take over the approval so that the assignee can merge after that!

The *dashboard contains a list for each of these four labels*, additionally to the default "open" and "closed" lists of the GitLab dashboard. Usually, an issue ticket moves from the dashboard lists on the left to the right lists, although an issue ticket does not necessarily pass any list, and it is also possible that it moves back to the previous state (e.g., from "Support needed" back to "In progress" is a realistic development). So, the *state label* changes during the progress of an issue ticket.

Unfortunately, the "Open Merge Requests" do not contain a dashboard, but they use the same labels in the same way! You can see all assigned labels of an open Merge Request at first glance when opening the "Open Merge Requests" page.

Additionally, there are *three labels that classify the type of an issue ticket*:

* Major change: 
This is a major change within Docs page(s). It needs a Merge Request and has to be approved by someone else than the assignee before it can be merged!

* Minor change: 
This is only a minor change in Docs page(s). It does not need an approval, and it can be committed directly (a Merge Request is not mandatory)!

* Internal task: 
This is an internal task of Fedora Docs -> every task that is not a change/fix/update of a Docs page/content! E.g., preparing a meeting or evaluating a survey.

Each issue ticket and each open Merge Request has to be labeled additionally with one of these three labels. The *type labels* are *persistent* (they do not change during the workflow) and determine how this issue ticket is handled in general. This includes the lists the issue ticket has to pass.

Beyond a state label and a type label, issue tickets and merge requests can be additionally labeled as "good first issue": You want to contribute to Fedora Docs? This is your opportunity! Feel free to comment in one of these issues that you want to take over! This is a good way to get in touch with Fedora Docs and the team, and to find out if you want to become yourself a member!

Further, there is an additional label for Merge Requests that could possibly affect more than one branch, which would mean that they require follow-up Merge Requests or follow-up cherry-picks (the latter is preferred): *"multiple MR likely"*. This label is added to ensure that all affected branches are identified and updated, and that Merge Requests that have this label are not merged without ensuring that the follow-ups are conducted and not forgotten (once accepted, a Merge Request disappears from the Open Merge Requests list, and remaining follow-ups are likely to become forgotten). 

Therefore, the "multiple MR likely" label implies that all affected branches have to be identified in advance to accepting any merge, whereas accepting the Merge Request and conducting the related cherry-picks have to be done at once. In short: it has to be ensured that the Merge Request does not disappear from the Open Merge Request page until the update of all affected branches is ensured. *It is no problem to add this label in cases of doubt.* It can be removed later, and it is better added once too often than reversed.

== How issue tickets and Merge Requests are created

There are two ways issue tickets and Merge Requests can be created: externally and internally.

* If created externally, they are created by people from outside the Fedora Docs team, who opened them in GitLab or using the "Report an issue" (creates issue tickets) / "Edit this page" (creates Merge Requests) buttons on any Fedora Docs page (the buttons are on the top right). Tickets will appear in the list "Open" on the dashboard without any labels. The Merge Requests will appear on the Open Merge Requests page, also without any labels.
* If created internally, one of the Docs Team opened it. At the best, this member already assigns a *type label* and puts issue tickets into the "Triage" list (this implies adding the "Triage" label), where the ticket can wait for an assignee to take over. It is the *same for Merge Requests*, although they do not have drag & drop lists and the "Triage" label has to be set *manually* just like the type label. Alternatively, the creating member can already assign an assignee to the issue ticket / Merge Request and then put (drag & drop) an issue ticket on the "In progress" list (this implies adding the "In progress" label) on the dashboard, or manually add the "In progress" label if it is a Merge Request. 

TIP: On the dashboard, issue tickets can be moved among the lists with drag and drop. The respective *state labels are changed automatically*. E.g., if a ticket is moved from "Triage" to "In progress" by drag and drop, the *state label* is changed automatically from "Triage" to "In progress"! Therefore, only the persistent *type labels* have to be set manually once when the ticket is new! This does **not** apply to Merge Requests on the Open Merge Request page.

TIP: When creating an issue ticket on the dashboard, you will be asked where to create the issue ticket. If you are *unsure where to create an issue ticket*, create it in fedora / Fedora Docs / Docs Website / Fedora Docs pages -> you will see this possibility in the "Projects" list ("Select a project") which is shown when you click to create an issue ticket. Later, this will be shown as "fedora/docs/docs-website/pages".

NOTE: In some circumstances, it is possible to simplify the workflow of "Minor changes" when it comes to creating/managing Merge Requests and issue tickets. See below. 

== The three workflows
=== "Minor change" types

If conducted by Docs members, and if it is clearly a "Minor change", a change can be done by a direct commit, without a Merge Request or issue ticket. If externals make a Merge Request which can be clearly classified as "Minor change", it is allowed that Docs members merge it immediately, after they *assigned themselves and reviewed* the Merge Request. But *always check* if a change (including Minor changes) applies to *multiple branches*! 

If multiple branches are affected by a "Minor change", you are only allowed to directly merge/commit if you do the merge/commit and the cherry-picks to all affected branches *at once*! *If you are unsure, add the "multiple MR likely" label* additionally to the "Minor change" label and keep the Merge Request open for discussions (you might use the "Support needed" label in such a situation: this label will be elaborated below). If nothing exists yet, open an issue ticket or a Merge Request (if you are unsure, open an issue ticket).

Depending on the following discussion, the "multiple MR likely" label will be removed if there are no other affected branches and then the merge/commit can be conducted, or if other branches are affected, the merge/commit can be conducted and a cherry-pick to all affected branches will be done *immediately and at once* with the merge/commit. The *goal* that has to be achieved here is: it has to be ensured that a Merge Request does *not disappear from the Open Merge Request page* until the update of *all* affected branches is ensured (once accepted, a Merge Request disappears from the Open Merge Requests list, and the follow-ups are likely to become forgotten). 

Obviously, if the discussion takes place within an issue ticket (using commits instead of Merge Requests, or using multiple Mege Requests that are converged within a ticket) and not within one Merge Request, the *commit and the cherry-picks can be done at different times*: the issue can be kept open until all work is done, and has to be closed manually after that.

TIP: Because of facts provided in the two above paragraphs, it can make sense to converge changes, which could affect multiple branches, in an issue ticket and not in a Merge Request: this makes it possible to split different branches' merges/commits so that different branches have not to be changed at once. This can be decided by the responsible Docs member based upon the situation.

NOTE: Despite the tip above, casual contributors are told in the 7-step HowTo to generally use the Merge Request if they want to propose a change and to ignore everything that has to do with branches: this aims to keep it short and simple for them. Nevertheless, they are of course free to create an issue ticket instead of Merge Requests.

In either case, if you create or manage a Merge Request/issue ticket, even if it is just a "Minor change", *assign yourself* as assignee to keep the documentation comprehensible, and to ensure that no one else starts managing it at the same time (avoiding duplication of work & confusions)!

Of course it is not obligated to process "Minor changes" immediately: but if you can spare a moment, feel free to just do the "Triage": check out what the Merge Request / issue ticket is about, add the applicable type label and possibly "multiple MR likely" or "good first issue" if applicable, and then move issue tickets to "Triage" / add the "Triage" label to Merge Requests. The labels ensure a meaningful overview for all members and add valuable information to the dashboard / to the Open Merge Request page. Generally, when you do a "Triage", if in doubt, *add more rather than less* ("major" instead of "minor"; additionally "multiple MR likely"; and so on).

The workflow for a change, which proves to be a *Minor change*, is as follows:

. If it is only a commit which clearly is a "Minor change", just do it. If you know in advance which branches are affected and if you can do the commit and all related cherry-picks immediately and at once, just do it. Then, you are done. If you have not sufficient time at the moment, and in all other cases, proceed as follows:

. Determine the type and assign the related type label ("Minor change", "Major change", "Internal task". Here: Minor change) to the existing Merge Request or the existing issue ticket, or if non is existing yet: create one! If you are unsure what to create, create an issue ticket! Also, add "multiple MR likely" if there is a possibility that this is applicable.

. Move new issues to the "Triage" list (which, as elaborated above, automatically assigns the "Triage" state label), or add the "Triage" label manually if it is a Merge Request. *If you just want to do the "Triage", you are done now!*

. Now, the issue ticket or the Merge Request can be assigned to a member who takes over. Once someone has been assigned, the ticket has to be moved to "In progress" (change to "In progress" has to be done manually for Merge Requests).

. Now there are three possibilities:
.. The assignee finishes the issue ticket/MR, and correspondingly, moves/puts it from the current state label to "Closed". If multiple branches are to be changed and if the case is handled within an MR, all branches have to be changed at once (as elaborated before). Type labels and additional labels shall remain.
.. Alternatively, the assignee needs support, or additional opinions: the ticket/MR is just moved to "Support needed" to identify supporter(s) (who can, but do not have to, be assigned as additional assignee(s) if that makes sense) and once sufficient supporter(s) have been identified, move/put it back to "In progress". "Support needed" can also be used to encourage a discussion in the ticket/MR, to get additional opinions or incentives. Once all work is finished, the ticket/MR can be moved/put to "Closed". If multiple branches are to be changed and if the case is handled within an MR, all branches have to be changed at once (as elaborated before). Type labels and additional labels shall remain. Members are *free to change* the responsible assignee (e.g., if someone else has more experience with the next task, or can invest more time).
.. If the assignee has to give up the assignment at all, the assignee is free to decide himself/herself what makes more sense in the given case: put it to "Support needed" and accompany the process of identifying a new assignee, or make a comment that sums up what was done and what remains to be done, remove the assignee and put it back to "Triage". It makes sense to put more urgent cases to "Support needed" and make other members aware of it (e.g., write additionally a comment in the Docs Matrix channel).

If a member identified the issue himself/herself and wants to do the change immediately and at once, it can be worked with commits and without ticket/MR as elaborated previously. "multiple MR likely" have to be considered as described above.

Discussions shall take place within the respective issue ticket or Merge Request.

If somewhere in the workflow the labeling proves to be mistaken, it is changed correspondingly, and so the following workflow.

IMPORTANT: The "stg" branch shall *not* be used for content/texts. You should work in dedicated forks/branches, which are just for the one issue you are working on, or commit directly to "main" if that is appropriate in the given case: in any case, the outcome of efforts should go directly to the "main" branch *without* "stg" in between.

TIP: Do not forget to rebase/update your fork/branch *regularly* in cases where contributions are not done by direct commits to "main". Do not wait too long before merging in order to avoid conflicts, which might force you to rebase conflicted changes manually (if conflicts appear, automated rebase will no longer work and it has to be done manually, which *cannot* be done within the GitLab IDE). Content/texts have no agency and thus, no dependencies: you can exploit this and split your work in flexible smaller pieces and commit/merge quickly one by one instead of working in long-standing huge MR. If direct commit is not sufficient, create a *dedicated fork* or a *dedicated branch* for your work, write the content, test it if necessary (e.g., using build.sh+preview.sh), merge, and then do the next piece of work. In short: keep forks and separated branches as close as possible to the downstream "main"!

=== "Major change" types

The handling of "multiple MR likely", as described for "Minor changes" above, can be *transferred* to "Major changes". However, "Major changes" *always* need an issue ticket or a Merge Request!

The workflow for a change, which proves to be a *Major change*, is as follows:

. Determine the type and assign the related type label ("Minor change", "Major change", "Internal task". Here: Major change) to the existing Merge Request or the existing issue ticket, or if non is existing yet: create one! If you are unsure what to create, create an issue ticket! Also, add "multiple MR likely" if there is a possibility that this is applicable.

. Move new issues to the "Triage" list (which, as elaborated above, automatically assigns the "Triage" state label), or add the "Triage" label manually if it is a Merge Request. *If you just want to do the "Triage", you are done now!*

. Now, the issue ticket or the Merge Request can be assigned to a member who takes over. Once someone has been assigned, the ticket has to be moved to "In progress" (change to "In progress" has to be done manually for Merge Requests).

. Now there are three possibilities:
.. The assignee *finishes actively working* on the issue ticket/MR, and moves/puts it from the current state label to *"Approval needed"*. 
.. Alternatively, the assignee needs support, or additional opinions: the ticket/MR is just moved to "Support needed" to identify supporter(s) (who can, but do not have to, be assigned as additional assignee(s) if that makes sense) and once sufficient supporter(s) have been identified, move/put it back to "In progress". "Support needed" can also be used to encourage a discussion in the ticket/MR, to get additional opinions or incentives. Once *actively working* on the ticket/MR is finished, the ticket/MR can be moved/put to *"Approval needed"*. Members are *free to change* the responsible assignee (e.g., if someone else has more experience with the next task, or can invest more time).
.. If the assignee has to give up the assignment at all, the assignee is free to decide himself/herself what makes more sense in the given case: put it to "Support needed" and accompany the process of identifying a new assignee, or make a comment that sums up what was done and what remains to be done, remove the assignee and put it back to "Triage". It makes sense to put more urgent cases to "Support needed" and make other members aware of it (e.g., write additionally a comment in the Docs Matrix channel).

. Now, a Docs member who is *not the assignee* has to approve the proposed changes. If it is an issue ticket, this can be done in the discussion. Within a Merge Request, additionally the button for approvals has to be used.

. Once a ticket/MR has been approved, the assignee is allowed to merge it! If multiple branches are to be changed and if the changes are handled within an MR, all branches have to be changed at once (see the note below). When all is done, it can be moved/put to "Closed"!

NOTE: If there is a *possibility that multiple branches are affected*, always add the "multiple MR likely" label. If this label is assigned, the discussion within the ticket/MR has to identify all affected branches. In the end, the Merge Request has to be transferred to all affected branches through *cherry-picks*. If multiple branches have to be changed, *all changes have to be done at once* (once accepted, a Merge Request disappears from the Open Merge Requests list, and the cherry-picks are likely to become forgotten). Thus, when the Merge Request becomes merged, the cherry-picks have to be done immediately. This does *not apply if the change is handled within an issue ticket*. The *goal* that has to be achieved here is: it has to be ensured that a Merge Request does *not disappear from the Open Merge Request page* until the update of *all* affected branches is ensured. *It is no problem to add this label in cases of doubt.* It can be removed later, and it is better added once too often than reversed.

Discussions shall take place within the respective issue ticket or Merge Request. "multiple MR likely" have to be considered as described previously.

If somewhere in the workflow the labeling proves to be mistaken, it is changed correspondingly, and so the following workflow.

TIP: Even if approved within an issue ticket, be careful with using commits for "Major changes". An additional MR makes it possible that the exact commit(s) can be reviewed by other members before merging because approval within an issue ticket does not avoid mistakes that occur in the commit. Therefore, an additional approval within an MR can make sense for bigger changes.

IMPORTANT: The "stg" branch shall *not* be used for content/texts. You should work in dedicated forks/branches, which are just for the one issue you are working on, or commit directly to "main" if that is appropriate in the given case: in any case, the outcome of efforts should go directly to the "main" branch *without* "stg" in between.

TIP: Do not forget to rebase/update your fork/branch *regularly* in cases where contributions are not done by direct commits to "main". Do not wait too long before merging in order to avoid conflicts, which might force you to rebase conflicted changes manually (if conflicts appear, automated rebase will no longer work and it has to be done manually, which *cannot* be done within the GitLab IDE). Content/texts have no agency and thus, no dependencies: you can exploit this and split your work in flexible smaller pieces and commit/merge quickly one by one instead of working in long-standing huge MR. If direct commit is not sufficient, create a *dedicated fork* or a *dedicated branch* for your work, write the content, test it if necessary (e.g., using build.sh+preview.sh), merge, and then do the next piece of work. In short: keep forks and separated branches as close as possible to the downstream "main"!

=== "Internal task" types 

Internal tasks are flexible. They might start in "Open" or "Triage", and move through "Triage", "In progress", "Support needed" (and maybe back) until they can be "Closed".

== How to create Merge Requests

The Docs team has no regulation about that. You are free to fork a repository and later make a Merge Request from your fork to the Docs repository (e.g., fork-main to Docs-main), or alternatively, you can create a new branch within the Docs repository and later merge from branch to branch (e.g., Docs-workBranch to Docs-main). You are free as long as your approach is comprehensible and facilitates the above workflows. Feel free to talk to other members if you are unsure.

Theoretically, the assignee should be always assigned to the Merge Requests just like they are assigned to the issue ticket. However, if a Merge Request is associated with an issue ticket, it is sufficient to have the assignee assigned to the issue ticket. Only Merge Requests that have no associated ticket need to be assigned.

== The role of the weekly meeting

The weekly meeting is to identify and tackle issues and tasks that are not ordinary or that do not occur regularly. The workflow organization on the other hand is to manage the standardized daily operations of Fedora Docs.

However, the current issue tickets and Merge Requests of the workflow organization can become regular topics on the weekly meeting: we have to regularly check if there are any unforeseen developments in the workflow that have to be discussed. Also, we have to identify and assign issue tickets and MR that remain unassigned for over **two weeks**, and discuss those that remain open **one month** after they have been assigned. Further, there can be thorny issue tickets and merge requests that need to be addressed and elaborated in a meeting before they can be completed: there is an *additional label "meeting"*, which can be assigned to issues/MR that need to be discussed in the next meeting. 

NOTE: The label "meeting" can be assigned by any member. As with the other labels, it is better to assign it once too often than too few.

To tackle these and other issues, the Fedora Docs team can decide to hold a monthly hackfest, if the weekly meeting proves to be not sufficient.
